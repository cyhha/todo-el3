import { ref } from "vue";
const counter = () => {
  const count = ref(0),
    add = () => count.value++;
  return { count, add };
};
export { counter };
