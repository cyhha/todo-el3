import { createStore } from "vuex";
import request from "../components/requests";
import { notEmpty } from "../components/utils";

// 创建一个全局状态仓库
const store = createStore({
  // 状态
  state() {
    return {
      user: {},
    };
  },
  // 修改状态方法
  mutations: {
    login(state, user) {
      state.user = user;
    },
    logout(state) {
      state.user = {};
    },
  },
  // 异步执行
  actions: {
    async isAuth({ state, commit }) {
      if (notEmpty(state.user)) return true;
      try {
        let { data } = await request("/user/info");
        if (data.success) commit("login", data.user);
        return data.success;
      } catch {
        return false;
      }
    },
  },
});

export default store;
