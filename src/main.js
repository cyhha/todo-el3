import { createApp } from "vue";
// element ui 导入
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import { ElNotification } from "element-plus";

// 根组件配置
import App from "./App.vue";
import "./app.css";
// router 路由设置
import router from "./router";
import store from "./store";

// 统一追加url的基础路径
import request, { createHttp, onHttpError } from "./components/requests";
createHttp({ baseURL: "/api" });
// 统一 Http Status Code Error 处理
onHttpError(({ status, message }) => {
  if (status === 401) {
    return router.push({ name: "login" });
  }
  ElNotification({
    title: "错误",
    message,
    type: "error",
  });
});

createApp(App)
  .use(store)
  .use(router)
  .use(request)
  .use(ElementPlus)
  .mount("#app");
