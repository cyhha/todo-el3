import { createRouter, createWebHashHistory } from "vue-router";

// 1. 定义路由组件. 也可以从其他文件导入
import Home from "../views/Home.vue";
import Login from "../views/Login.vue";
import Register from "../views/Register.vue";
// 动态导入页面组件
const About = () => import("../views/About.vue");
// 导入全局状态 store
import store from "../store";

// 2. 定义一些路由
// 每个路由都需要映射到一个组件。
// 我们后面再讨论嵌套路由。
const routes = [
  { name: "home", path: "/", component: Home },
  { name: "login", path: "/login", component: Login },
  { name: "register", path: "/register", component: Register },
  { name: "about", path: "/about", component: About },
];
// 3. 创建路由实例并传递 `routes` 配置
// 你可以在这里输入更多的配置，但我们在这里
// 暂时保持简单
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
});
// 全局路由守卫
router.beforeEach(async (to, from, next) => {
  if (
    !["login", "register"].includes(to.name) &&
    !(await store.dispatch("isAuth"))
  ) {
    next({ name: "login" });
  } else next();
});
export default router;
